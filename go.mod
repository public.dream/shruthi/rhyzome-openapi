module git.callpipe.com/entanglement.garden/rhyzome-openapi

go 1.15

require (
	github.com/deepmap/oapi-codegen v1.10.1
	github.com/getkin/kin-openapi v0.94.0
	github.com/labstack/echo/v4 v4.7.2
)
