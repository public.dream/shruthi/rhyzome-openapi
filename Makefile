all: rhyzome-client.gen.go rhyzome-server.gen.go rhyzome-spec.gen.go rhyzome-types.gen.go

rhyzome-client.gen.go: rhyzome.yaml
	oapi-codegen -generate client -o rhyzome-client.gen.go -package rhyzome rhyzome.yaml

rhyzome-server.gen.go: rhyzome.yaml
	oapi-codegen -generate server -o rhyzome-server.gen.go -package rhyzome rhyzome.yaml

rhyzome-spec.gen.go: rhyzome.yaml
	oapi-codegen -generate spec -o rhyzome-spec.gen.go -package rhyzome rhyzome.yaml

rhyzome-types.gen.go: rhyzome.yaml
	oapi-codegen -generate types -o rhyzome-types.gen.go -package rhyzome rhyzome.yaml

doc: rhyzome.yaml
	@docker run --rm -v $(PWD):/local openapitools/openapi-generator:cli-latest-release generate \
		-i /local/rhyzome.yaml -g html -o /local/doc
