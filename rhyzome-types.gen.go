// Package rhyzome provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen version v1.10.1 DO NOT EDIT.
package rhyzome

// Defines values for InstanceState.
const (
	InstanceStateCREATING InstanceState = "CREATING"

	InstanceStateDELETED InstanceState = "DELETED"

	InstanceStateDELETING InstanceState = "DELETING"

	InstanceStateERROR InstanceState = "ERROR"

	InstanceStatePENDING InstanceState = "PENDING"

	InstanceStateRUNNING InstanceState = "RUNNING"

	InstanceStateSTOPPED InstanceState = "STOPPED"
)

// Defines values for JobState.
const (
	JobStateDONE JobState = "DONE"

	JobStateFAILED JobState = "FAILED"

	JobStateQUEUED JobState = "QUEUED"

	JobStateRUNNING JobState = "RUNNING"
)

// CreateInstanceRequestBody defines model for CreateInstanceRequestBody.
type CreateInstanceRequestBody struct {
	BaseImage string  `json:"base_image"`
	Cpu       int     `json:"cpu"`
	DiskSize  int     `json:"disk_size"`
	Memory    int     `json:"memory"`
	UserData  *string `json:"user_data,omitempty"`
}

// CreateInstanceResponseBody defines model for CreateInstanceResponseBody.
type CreateInstanceResponseBody struct {
	InstanceId string `json:"instance_id"`
	JobId      string `json:"job_id"`
}

// Error defines model for Error.
type Error struct {
	Code    int32  `json:"code"`
	Message string `json:"message"`
}

// Instance defines model for Instance.
type Instance struct {
	Cpu        int           `json:"cpu"`
	Memory     int           `json:"memory"`
	Node       string        `json:"node"`
	ResourceId string        `json:"resource_id"`
	RootVolume string        `json:"root_volume"`
	State      InstanceState `json:"state"`
	UserData   *string       `json:"user_data,omitempty"`
}

// InstanceState defines model for Instance.State.
type InstanceState string

// Job defines model for Job.
type Job struct {
	CreatedAt int64    `json:"created_at"`
	JobId     string   `json:"job_id"`
	JobType   string   `json:"job_type"`
	State     JobState `json:"state"`
	StateText string   `json:"state_text"`
}

// JobState defines model for Job.State.
type JobState string

// JobRef defines model for JobRef.
type JobRef struct {
	JobId string `json:"job_id"`
}

// Network defines model for Network.
type Network struct {
	Id string `json:"id"`
}

// ProvisioningEndpoints defines model for ProvisioningEndpoints.
type ProvisioningEndpoints struct {
	Grpc string `json:"grpc"`
	Step string `json:"step"`
}

// ProvisioningInfo defines model for ProvisioningInfo.
type ProvisioningInfo struct {
	CaFingerprint string                `json:"ca_fingerprint"`
	Endpoints     ProvisioningEndpoints `json:"endpoints"`
}

// CreateInstanceJSONBody defines parameters for CreateInstance.
type CreateInstanceJSONBody CreateInstanceRequestBody

// CreateInstanceJSONRequestBody defines body for CreateInstance for application/json ContentType.
type CreateInstanceJSONRequestBody CreateInstanceJSONBody
