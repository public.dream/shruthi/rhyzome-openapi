# Rhyzome OpenAPI

Common OpenAPI files shared between Rhyzome components.

## Documentation

Run: `make doc` to generate the OpenAPI documentation in [`./doc/`](./doc/).
